import RPi.GPIO as GPIO			# Import GPIO library	
import time						# Import time library, allows us to use "sleep" function

GPIO.setmode (GPIO.BOARD)		# Use Board pin numbering
pinLED = 10						# Define pin to control LED
GPIO.setup(pinLED, GPIO.OUT)	# Setup GPIO pinLED to OUTPUT mode
 
# Define a function named Blink() to implement blinking a LED
def Blink(numTimes, speed):					# Define Blink() function with 2 input variables
	for i in range(0, numTimes):				# Run the loop for numTimes (number of times to run blink() function)
		print("Iteration"+str(i+1))			# Print current loop

		GPIO.output(pinLED, True)			# Turn on GPIO pinLED
		time.sleep(speed)					# Wait an interval = speed

		GPIO.output(pinLED, False)			# Turn off GPIO pinLED
		time.sleep(speed)					# Wait an interval = speed

	print ("Done")							# Print "Done" when the loop is completed
	GPIO.cleanup()							# Clear GPIO status

# Prompt user for input parameters
iterations = input("Enter the total number of times to blink: ")
speed = input("Enter the length of each blink in seconds: ")

# Start Blink() function
Blink(int(iterations), float(speed))
