import RPi.GPIO as GPIO				# Import GPIO library	
import time							# Import time library

# Define a initialization function to set up pin controlling the DC motor
def init():
	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(16, GPIO.OUT)
	GPIO.setup(18, GPIO.OUT)

# Define a function to control the DC motor moving forward
def forward(tf):
	GPIO.output(16, False)
	GPIO.output(18, True)
	time.sleep(tf)

# Define a function to control the DC motor moving backward
def reverse(tf):
	GPIO.output(16, True)
	GPIO.output(18, False)
	time.sleep(tf)

# Infinite loop
while True:
	init()
	print ("Move forward")
	forward(5)					# The DC motor moves forward in 5s
	print("reverse")
	reverse(5)					# The DC motor moves backward in 5s
	GPIO.cleanup()