import sys
import spidev
import time

spi = spidev.SpiDev()
spi.open()

# read SPI data from one of the MCP3008's eight possible inputs (0 --> 7)
def readadc(adcnumb):
	if adcnumb > 7 or adcnumb < 0:
		return -1

	readout = spi.xfer([1, 8 + adcnumb <<4, 0])
	adcout = ((readout[1] & 3) << 8) + readout[2]
	return adcout

def raw2temp(raw):
	millivolts = raw * (3.3 * 100 / 1023.0)
	tempk = millivolts
	tempc = millivolts - 273.15
	tempf = tempc * 9.0 / 5.0 + 32
	return(tempk, tempc, tempf)


sensor0pin = 0

try:
	while True:
		rawval0 = readadc(sensor0pin)

		# Convert the raw ADC input to millivolts,
		# degrees Celsius and Fahrenheit
		(temp_kelvins, temp_celsius, temp_fahrenheit) = raw2temp(rawval0)
		print (
			'LM335 Sensor 0: ', 
			'raw = ', rawval0,
			'Kelvins = ', '{0:.1f}'.format(temp_kelvins),
			'Celsius = ', '{0:.1f}'.format(temp_celsius),
			'Fahrenheit', '{0:.1f}'.format(temp_fahrenheit),
			)

		time.sleep(1)

except KeyboardInterrupt:
	spi.close()
	sys.exit(0)