import RPi.GPIO as GPIO			# Import GPIO library	
import time						# Import time library, allows us to use "sleep" function

GPIO.setmode (GPIO.BOARD)		# Use Board pin numbering
pinLEDs = [7, 11, 13]

# Setup pin mode for pinLEDs
print ("Setup pins to control")
for pin in pinLEDs:
	GPIO.setup(pin, GPIO.OUT)

# Create patterns of three LEDs
parttern = []
parttern = range(0, 3)
parttern[0] = [1, 0, 0]
parttern[1] = [0, 1, 0]
parttern[2] = [0, 0, 1]

counter = 0		
waitSec = 0.05

# Excecute patterns in an infinite loop
while True:
	for idx in range(0, 3):
		xpin = pinLEDs[idx]

		if parttern[counter][idx] == 1:
			# you have to place your command here

		else:
			# you have to place your command here

	counter +=1				# Move to next parttern
	if(counter > 2):		# Return to the first pattern
		counter = 0

	time.sleep(waitSec)