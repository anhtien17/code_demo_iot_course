import RPi.GPIO as GPIO			# Import GPIO library	
import time						# Import time library, allows us to use "sleep" function

GPIO.setmode (GPIO.BOARD)		# Use Board pin numbering
pinLED = 10						# Define pin to control LED
GPIO.setup(pinLED, GPIO.OUT)	# Setup GPIO pinLED to OUTPUT mode

speed = 0.5;
print("The LED light blinks with the speed: "+str(speed)+ " second")			# Print current loop

# Run an infinite loop
while True:					# Define Blink() function with 2 input variables
   
        GPIO.output(pinLED, True)			# Turn on GPIO pinLED
        time.sleep(speed)				# Wait an interval = speed

        GPIO.output(pinLED, False)			# Turn off GPIO pinLED
        time.sleep(speed)				# Wait an interval = speed

